# Image Aesthetic Scoring Service (v3)

This is the code repository of a web service for scoring images based on their aesthetic value.

## 1. Python version

3.6 & up 

## 2. Setup

### Server

1. Create and activate Python virtual environment
2. Clone `gilfoyle` and follow the instructions in the repo.
3. Install Python dependencies `pip install -r requirements_install.txt`
4. Download model artifacts
    1. `model-final-cycle2-resnext50_32x4d_weightsimagenet_photocrowd_stloss_snapshots5x15.pth` from `s3://sagemaker-gx-img-aes/models/` and put into directory `models/.`
    2. `model-final-cycle3-resnext50_32x4d_weightsimagenet_photocrowd_stloss_snapshots5x15.pth` from `s3://sagemaker-gx-img-aes/models/` and put into directory `models/.`
    3. `model-final-cycle4-resnext50_32x4d_weightsimagenet_photocrowd_stloss_snapshots5x15.pth` from `s3://sagemaker-gx-img-aes/models/` and put into directory `models/.`

## 3. Run

1) Activate Redis Server `redis-server` and flush `redis-cli FLUSHALL`

2) Run Python scripts

i) `python run_web_server.py`

ii) `python run_model_server.py`