from gilfoyle.microservice.configuration import FrameworkConfiguration, get_default_json_config
import dacite 
import logging.config

if __name__ == '__main__':

	default_config = get_default_json_config()
	overwrite_config = {'version': {'version': '0.1'}}
	default_config.update(overwrite_config)	
	config = dacite.from_dict(
		data_class=FrameworkConfiguration,
		data=default_config,
		config=dacite.Config())