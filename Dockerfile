FROM python:3.6.7

WORKDIR /usr/src/app

COPY . .

RUN pip install -r gilfoyle/requirements.txt
RUN pip uninstall -y pillow
RUN CC="cc -mavx2" && pip install -U --force-reinstall pillow-simd
RUN pip install -r requirements_install.txt

ENTRYPOINT ['python3']
