import base64
import requests 
import json 
from threading import Thread
import time 
import random
import string

def get_random_alphaNumeric_string(stringLength=8):
    lettersAndDigits = string.ascii_letters + string.digits
    return ''.join((random.choice(lettersAndDigits) for i in range(stringLength)))

def call_predict_endpoint(n):
	payload = valid_request if n%2==0 else invalid_request
	if n % 2 == 0:
		print('Sending valid request')
		payload = valid_request
	else:
		print('Sending invalid request')
		payload = invalid_request

	try:
		r = requests.post(url, json={'image': payload})
		print(r)
		print(r.json())
		print(f'{n} thread done')
	except Exception as e:
		print(e)


if __name__ == '__main__':
	num_requests = 10
	image_file_path = 'images/mixed-fruits.jpg'

	invalid_request = get_random_alphaNumeric_string()
	with open(image_file_path, 'rb') as f:
		base64_img = base64.b64encode(f.read())
	valid_request = base64_img.decode('utf-8')

	url ="http://0.0.0.0:82/inference"
	# url="http://localhost:5000/inference"
	# r = requests.post(url, json={'img': base64_img.decode('utf-8')}).json()

	for i in range(num_requests):
		t = Thread(target = call_predict_endpoint, args=(i,))
		t.daemon = True
		t.start()
		time.sleep(0.05)
	time.sleep(300)
	
	# for i in range(num_requests):
	# 	call_predict_endpoint(i)