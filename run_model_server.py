import pickle
import json
import redis
import logging
logging.basicConfig(level=logging.INFO)
import logging.config 
logging.config.dictConfig({
		"version": 1,
		"formatters": {
			"simple" : {
				"format": "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
			}
		},
		"handlers": {
			"console": {
				"class": "logging.StreamHandler",
				"level": "INFO",
				"formatter": "simple",
				"stream": "ext://sys.stdout"
			}
		},
		"loggers": {
			"sampleLogger": {
				"level": "INFO",
				"handlers": ["console"],
				"propagate": "no"
			}
		},
		"root": {
			"level": "INFO",
			"handlers": ["console"]
		}
})
from convert_to_api import ImageAestheticsV3

db = redis.StrictRedis(host="redis", port=6379, db=0)

def inference_process():

	# Instantiate model into inference model
	with open('aesthetics_v3_artifacts/model.pickle', 'rb') as f:
		model = pickle.load(f)

	model.inference_init()

	while True:
		queue = db.lrange('request_queue', 0, model.config.batch_size)
		queue = [json.loads(q.decode('utf-8')) for q in queue]
		if len(queue) > 0:
			results = model.inference(queue)

			for result in results:
				db.set(result['id'], json.dumps(result))
				db.ltrim('request_queue', 1, -1)

if __name__ == '__main__':
	inference_process()