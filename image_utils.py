import base64

import albumentations as albu
import albumentations.pytorch
import cv2
import numpy as np


def load_image(path: str) -> np.ndarray:
    """Load an image given its path.

    Arguments:
        path {str} -- Path of the image.

    Returns:
        np.ndarray -- The loaded image.
    """

    img = cv2.imread(path, cv2.IMREAD_COLOR)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    return img


def get_transforms(width, height):
    return albu.Compose([
        albu.Resize(height=height,
                    width=width,
                    interpolation=cv2.INTER_CUBIC,
                    p=1.0),

        albumentations.pytorch.ToTensor()
    ], p=1.0)


def raw_to_base64(path: str):
    """Base64 encode the raw binary contents of a file.

    Arguments:
        path {str} -- The path of the file.

    Returns:
        [bytes] -- The byte stream of the encoded contents.
    """

    with open(path, "rb") as f:
        data = base64.b64encode(f.read())

    return data
