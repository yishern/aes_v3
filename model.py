import logging
import typing
from collections import OrderedDict

import torch
import torch.nn as nn
import torchvision.models as models
from efficientnet_pytorch import EfficientNet

logger = logging.getLogger("aesthetic_model_service.model")


def model_fn(backbone: str, use_pretrained: bool = True, num_classes: int = 1) -> nn.Module:
    """Instantiate model.
    """

    # Initialize these variables which will be set in this if statement. Each of these
    #   variables is model specific.
    model_ft = None

    logger.info(
        f"model - backbone: {backbone}, use_pretrained: {use_pretrained}, num_classes: {num_classes}")

    if backbone == "resnet18":
        model_ft = models.resnet18(pretrained=use_pretrained)
        num_ftrs = model_ft.fc.in_features
        model_ft.fc = nn.Linear(num_ftrs, num_classes)

    elif backbone == "resnet34":
        model_ft = models.resnet34(pretrained=use_pretrained)
        num_ftrs = model_ft.fc.in_features
        model_ft.fc = nn.Linear(num_ftrs, num_classes)

    elif backbone == "resnet50":
        model_ft = models.resnet50(pretrained=use_pretrained)
        num_ftrs = model_ft.fc.in_features
        model_ft.fc = nn.Linear(num_ftrs, num_classes)

    elif backbone == "resnext50_32x4d":
        model_ft = models.resnext50_32x4d(pretrained=use_pretrained)
        num_ftrs = model_ft.fc.in_features
        model_ft.fc = nn.Linear(num_ftrs, num_classes)

    elif backbone == "resnext101_32x8d":
        model_ft = models.resnext101_32x8d(pretrained=use_pretrained)
        num_ftrs = model_ft.fc.in_features
        model_ft.fc = nn.Linear(num_ftrs, num_classes)

    elif backbone == "wide_resnet50_2":
        model_ft = models.wide_resnet50_2(pretrained=use_pretrained)
        num_ftrs = model_ft.fc.in_features
        model_ft.fc = nn.Linear(num_ftrs, num_classes)

    elif backbone == "wide_resnet101_2":
        model_ft = models.wide_resnet101_2(pretrained=use_pretrained)
        num_ftrs = model_ft.fc.in_features
        model_ft.fc = nn.Linear(num_ftrs, num_classes)

    elif backbone == "alexnet":
        model_ft = models.alexnet(pretrained=use_pretrained)
        num_ftrs = model_ft.classifier[6].in_features
        model_ft.classifier[6] = nn.Linear(num_ftrs, num_classes)

    elif backbone == "vgg16":
        model_ft = models.vgg16(pretrained=use_pretrained)
        num_ftrs = model_ft.classifier[6].in_features
        model_ft.classifier[6] = nn.Linear(num_ftrs, num_classes)

    elif backbone == "vgg19":
        model_ft = models.vgg19(pretrained=use_pretrained)
        num_ftrs = model_ft.classifier[6].in_features
        model_ft.classifier[6] = nn.Linear(num_ftrs, num_classes)

    elif backbone == "squeezenet1_0":
        model_ft = models.squeezenet1_0(pretrained=use_pretrained)
        model_ft.classifier[1] = nn.Conv2d(
            512, num_classes, kernel_size=(1, 1), stride=(1, 1))
        model_ft.num_classes = num_classes

    elif backbone == "densenet121":
        model_ft = models.densenet121(pretrained=use_pretrained)
        num_ftrs = model_ft.classifier.in_features
        model_ft.classifier = nn.Linear(num_ftrs, num_classes)

    elif backbone == "densenet161":
        model_ft = models.densenet161(pretrained=use_pretrained)
        num_ftrs = model_ft.classifier.in_features
        model_ft.classifier = nn.Linear(num_ftrs, num_classes)

    elif backbone == "densenet169":
        model_ft = models.densenet169(pretrained=use_pretrained)
        num_ftrs = model_ft.classifier.in_features
        model_ft.classifier = nn.Linear(num_ftrs, num_classes)

    elif backbone == "densenet201":
        model_ft = models.densenet201(pretrained=use_pretrained)
        num_ftrs = model_ft.classifier.in_features
        model_ft.classifier = nn.Linear(num_ftrs, num_classes)

    elif backbone == "inception_v3":
        """ Inception v3
        Be careful, expects (299,299) sized images and has auxiliary output
        """
        model_ft = models.inception_v3(pretrained=use_pretrained)
        # Handle the auxilary net
        num_ftrs = model_ft.AuxLogits.fc.in_features
        model_ft.AuxLogits.fc = nn.Linear(num_ftrs, num_classes)
        # Handle the primary net
        num_ftrs = model_ft.fc.in_features
        model_ft.fc = nn.Linear(num_ftrs, num_classes)

    elif backbone.startswith("efficientnet"):
        model_ft = EfficientNet.from_pretrained(
            backbone, num_classes=num_classes)
        model_ft.set_swish(memory_efficient=False)
        # num_ftrs = model_ft._fc.in_features
        model_ft._swish = nn.Identity()
        # print(model_ft)
        # exit()
        # model_ft = nn.Sequential(*list(model_ft.children())[:-2],
        #                          nn.Linear(num_ftrs, num_classes))

    else:
        logger.error(f"Invalid backbone name ({backbone}), exiting...")
        exit()

    return model_ft


class ImageAestheticEnsembleModel(nn.Module):
    """An ensemble CNN regressor for scoring of images."""

    def __init__(self):
        super(ImageAestheticEnsembleModel, self).__init__()

        # Ensemble of three models
        self.model_0 = model_fn(backbone="resnext50_32x4d",
                                use_pretrained=False,  # we will load our own weights later
                                num_classes=1)
        self.model_1 = model_fn(backbone="resnext50_32x4d",
                                use_pretrained=False,  # we will load our own weights later
                                num_classes=1)
        self.model_2 = model_fn(backbone="resnext50_32x4d",
                                use_pretrained=False,  # we will load our own weights later
                                num_classes=1)

    def forward(self, x):
        # Perform forward pass for each member model
        score_model_0 = self.model_0(x)
        score_model_1 = self.model_1(x)
        score_model_2 = self.model_2(x)

        # Concatenate the predictions and perform averaging
        scores = torch.cat([score_model_0, score_model_1, score_model_2],
                           dim=-1)
        score_avg = scores.mean(dim=-1)

        return score_avg


def get_model_state_dict(checkpoint_path: str, device: str = "cuda") -> OrderedDict:
    """When we train PyTorch models using nn.DataParallel(),
    the names of the parameters are prepended with "module.",
    which causes problem in loading model state dict when nn.DataParallel() is not used.

    To overcome this problem, we create a new dict and rename the keys
    in the original dictionary while keeping the values.

    Arguments:
        checkpoint_path {str} -- PyTorch checkpoint file path
        device {str} -- device placement

    Returns:
        OrderedDict -- model state dict
    """

    checkpoint = torch.load(checkpoint_path, map_location=device)

    new_state_dict = OrderedDict()

    if "best_model_state_dict" in checkpoint:
        best_model_state_dict = checkpoint['best_model_state_dict']

        for k, v in best_model_state_dict.items():
            if k.startswith("module."):
                k = k.replace("module.", "")

            new_state_dict[k] = v
    else:
        best_model_state_dict = checkpoint

        for k, v in best_model_state_dict.items():
            if k.startswith("module."):
                k = k.replace("module.", "")

            new_state_dict[k] = v

    return new_state_dict


def load_model(device: typing.Optional[str] = "cuda") -> nn.Module:
    """Loads the image aesthetic model.

    Keyword Arguments:
        device {typing.Optional[str]} -- model placement: "cuda", "cpu", or None (default: {"cuda"})

    Returns:
        nn.Module -- An instance of the image aesthetic model.
    """

    if not device:
        device = "cuda" if torch.cuda.is_available() else "cpu"
    if "cuda" in device:
        device = "cuda" if torch.cuda.is_available() else "cpu"

    # Instantiate model and move it to device
    model = ImageAestheticEnsembleModel()
    model.to(device)

    # Load weights
    model.model_0.load_state_dict(get_model_state_dict(
        "models/model-final-cycle2-resnext50_32x4d_weightsimagenet_photocrowd_stloss_snapshots5x15.pth",
        device=device))
    model.model_1.load_state_dict(get_model_state_dict(
        "models/model-final-cycle3-resnext50_32x4d_weightsimagenet_photocrowd_stloss_snapshots5x15.pth",
        device=device))
    model.model_2.load_state_dict(get_model_state_dict(
        "models/model-final-cycle4-resnext50_32x4d_weightsimagenet_photocrowd_stloss_snapshots5x15.pth",
        device=device))

    cuda_device_count = torch.cuda.device_count()
    if cuda_device_count > 1:
        logger.info(
            f"Multiple CUDA devices ({cuda_device_count}) available, using data parallelism...")
        model = nn.DataParallel(model)

    # Set model to the correct mode
    model.eval()

    return model
