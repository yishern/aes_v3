from dataclasses import dataclass 

@dataclass 
class AestheticV3Configuration:
	height: int
	width: int
	batch_size: int
	device: str