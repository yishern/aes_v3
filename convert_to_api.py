from image_utils import get_transforms
# from misc_utils import base64_to_image
from model import load_model
import torch
import numpy as np
import base64
from gilfoyle.microservice import MicroserviceBackbone, MicroserviceWrapper
from gilfoyle.utils.image import base64_decode_image, base64_to_pil
from config import AestheticV3Configuration
import dacite
import argparse 
import json

class ImageAestheticsV3(MicroserviceBackbone):
	def __init__(self, config, **kwargs):
		MicroserviceBackbone.__init__(self, **kwargs)
		self.config = config 

	def preprocess_init(self):
		self.transform = get_transforms(width=self.config.width, height=self.config.height)

	def inference_init(self):
		self.model = load_model(device=self.config.device)

	def preprocess(self, x):
		img = x['image']
		image = np.array(base64_to_pil(img))
		preprocessed_img = self.transform(image=image)['image'].numpy()
		preprocessed_img = preprocessed_img.copy(order="C")
		return {'image': base64.b64encode(preprocessed_img).decode('utf-8')}

	def inference(self, x):
		self.logger.info(f'retrieved requests of length {len(x)} to be inferenced')
		ids = []
		images = []
		for _x in x: 
			ids.append(_x['id'])
			img = _x['image']
			img = base64_decode_image(img, dtype='float32', shape=(3, self.config.height, self.config.width))
			images.append(torch.from_numpy(img))

		images = torch.stack(images,0)
		images = images.to(self.config.device)

		with torch.no_grad():
			predictions = self.model(images)

		if 'cuda' in self.config.device:
			predictions = predictions.cpu()

		predictions = predictions.detach().numpy()
		results = []
		for (_id, prediction) in zip(ids, predictions):
			results.append({'id': _id, 'prediction': prediction.tolist()})
		self.logger.info(f'Inferenced and returning results of length {len(results)} \n')
		return results 

if __name__ == '__main__':
	ap = argparse.ArgumentParser()
	ap.add_argument('--config', type=str, default='config/image_aesthetics_v3.json',
		help='Path to JSON file containing model parameters')
	args = ap.parse_args()
	

	with open(args.config, 'r') as f:
		json_config = json.load(f)

	model_config = dacite.from_dict(
		data_class=AestheticV3Configuration,
		data=json_config,
		config=dacite.Config())

	framework_config = {'name': 'ImageAestheticsV3'}
	model = ImageAestheticsV3(model_config, framework_config=framework_config)

	wrapper = MicroserviceWrapper(model=model, microservice_config={'logging_level': 'info'}, save_directory='aesthetics_v3_artifacts')
	wrapper.serialize()